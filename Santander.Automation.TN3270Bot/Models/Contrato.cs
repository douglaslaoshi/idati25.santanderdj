﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Santander.Automation.TN3270Bot.Models
{
    public class Contrato
    {
        public string contrato;
        public string decisao;
        public string motivo;
        public string desconto;
        public string sigla;
        public string taxa;
        
    }
}
