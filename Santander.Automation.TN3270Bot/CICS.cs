﻿using Open3270;
using Santander.Automation.TN3270Bot.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Santander.Automation.TN3270Bot
{
    public class CICS : IAudit
    {
        private int _timeout = 180000;   
        private int _maxRetries = 20;

        public TNEmulator ConnectAndLogin(string user, string pass, string ip, int port)
        {

        
            

            TNEmulator emulator = new TNEmulator();
            emulator.Audit = this;
            emulator.Debug = false;
            emulator.Config.TermType = "IBM-3278-2-E";
            emulator.Config.FastScreenMode = true;
            emulator.Connect(ip, port, null);
            if (!emulator.WaitForText(0, 5, "THIS TERMINAL IS LOGGED ON", _timeout))
                throw new Exception("Erro ao logar, não encontrada mensagem: THIS TERMINAL IS LOGGED ON");
            emulator.SendText("0cicabnpt");
            emulator.SendKey(true, TnKey.Enter, _timeout);
            emulator.SendKey(true, TnKey.Enter, _timeout);

            if (!emulator.WaitForText(1, 2, "WELCOME TO CICS", _timeout))
                throw new Exception("Erro ao logar, não encontrada mensagem: WELCOME TO CICS");

            WriteTextToField(emulator.CurrentScreenXML.Fields[18], emulator, user);
            WriteTextToField(emulator.CurrentScreenXML.Fields[25], emulator, pass);
            emulator.SendKey(true, TnKey.Enter, _timeout);
            emulator.WaitForHostSettle(200, _timeout);
            emulator.SendKey(true, TnKey.Enter, _timeout);

            if (!emulator.WaitForText(5, 0, "CICS-SSO", _timeout))
                throw new Exception("Erro ao logar, não encontrada mensagem: CICS-SSO");
            return emulator;
        }


        public void ResetToMainMenu(TNEmulator emulator)
        {
            int count = 0;
            while (count <= _maxRetries)
            {
                emulator.SendKey(true, TnKey.F3, _timeout);
                emulator.WaitForHostSettle(200, _timeout);
                if (emulator.CurrentScreenXML.Dump().Contains("Aymore Financiamentos"))
                    return;
                count++;
            }
            throw new Exception("Erro ao retornar ao menu principal, não encontrada mensagem: Aymore Financiamentos");
        }

        public List<Contrato> AutorizarDispensaJuros(TNEmulator emulator)
        {
            List<Contrato> contratosAprovados = new List<Contrato>();
            ResetToMainMenu(emulator);
            string menuOption = "";
            for (int i = 0; i < emulator.CurrentScreenXML.Fields.Length; i++)
            {
                if (emulator.CurrentScreenXML.Fields[i].Text != null &&
                    emulator.CurrentScreenXML.Fields[i].Text.Contains("RPC - "))
                {
                    menuOption = emulator.CurrentScreenXML.Fields[i - 1].Text;
                    break;
                }
            }
            if (string.IsNullOrEmpty(menuOption))
                throw new Exception("Erro ao autorizar despesa, não foi possível encontrar a opção do menu");
            emulator.SetText(menuOption);
            emulator.SendKey(true, TnKey.Enter, _timeout);
            emulator.SendKey(true, TnKey.Enter, _timeout);
            if (!emulator.WaitForText(27, 1, "ATENDIMENTO AO CLIENTE", _timeout))
                throw new Exception("Erro ao logar, não encontrada mensagem: ATENDIMENTO AO CLIENTE");
            emulator.WaitForHostSettle(200, _timeout);
            emulator.SetText("16");
            emulator.SendKey(true, TnKey.Enter, _timeout);
            WriteTextToField(emulator.CurrentScreenXML.Fields[18], emulator, "10");
            WriteTextToField(emulator.CurrentScreenXML.Fields[38], emulator, "i");
            WriteTextToField(emulator.CurrentScreenXML.Fields[46], emulator, "2");
            WriteTextToField(emulator.CurrentScreenXML.Fields[67], emulator, "1235");
            if (!emulator.SendKey(true, TnKey.Enter, _timeout))
                throw new Exception("Timeout ao entrar na tela de dispensa de juros");
            if (String.IsNullOrEmpty(emulator.CurrentScreenXML.Fields[45].Text))
            {
                Console.Clear();
                Console.Write(emulator.CurrentScreenXML.Dump());
                //return contratosAprovados;
            }
            else
            {

                //12 linhas, com 12 campos por linha
                for (int i = 45; i < 45 + (12 * 12); i += 12)
                {
                    //i = index contrato
                    //aprovacao = i-1
                    //taxa = i+7
                    if (!String.IsNullOrEmpty(emulator.CurrentScreenXML.Fields[i].Text))
                    {
                        if (float.TryParse(emulator.CurrentScreenXML.Fields[i + 7].Text, out float taxa))
                        {
                            if (taxa <= 100)
                            {
                                Contrato contrato = new Contrato();
                                contrato.contrato = emulator.CurrentScreenXML.Fields[i].Text;
                                contrato.decisao = "APROVADO";
                                contrato.motivo = emulator.CurrentScreenXML.Fields[i + 5].Text;
                                contrato.desconto = emulator.CurrentScreenXML.Fields[i + 4].Text;
                                contrato.taxa = taxa.ToString();
                                contratosAprovados.Add(contrato);
								WriteTextToField(emulator.CurrentScreenXML.Fields[i - 1], emulator, "a");
							}
                            else
                            {

                                // DESCOMENTAR PARA REPROVAR SE FOR MAIOR QUE 100%
                                //WriteTextToField(emulator.CurrentScreenXML.Fields[i - 1], emulator, "r");
                                //contratosAprovados.Add("r"+emulator.CurrentScreenXML.Fields[i].Text);
                            }
                        }
                    }



                }
                Console.Clear();
				
                Console.Write(emulator.CurrentScreenXML.Dump());
			

				if (contratosAprovados.Any())
                {
                    emulator.SendKey(true, TnKey.F5, _timeout);
                }
				}
							//Licença por data de uso *************************************************************************************************


			StreamReader sr = File.OpenText("01010001");



			System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
			System.Text.Decoder utf8Decode = encoder.GetDecoder();
			byte[] todecode_byte = Convert.FromBase64String(sr.ReadLine());
			int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
			char[] decoded_char = new char[charCount];
			utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
			string result = new String(decoded_char);

			DateTime data = Convert.ToDateTime(result);
			DateTime lic = DateTime.Now;

			if (lic >= data)
			{
				int mess = 0;
				while (mess++ < mess)
				{
					Console.WriteLine("Licença expirada," + data + " favor informar a supervisão");
				}

			}
			else
			{
				Console.WriteLine("                                                  ");
				Console.WriteLine("                                                  ");
				Console.WriteLine("********************" +
					"****  Senha do Login Expira 07/02/2019  *********************");
				Console.WriteLine("                                                  ");

				Console.WriteLine("********************" +


					"****  Licença Expira 19/02/2019  ****************************");
				Console.WriteLine("                                                  ");
				Console.WriteLine("                                                  ");

				return contratosAprovados;
            }

            return contratosAprovados;

        }

     
        private void WriteTextToField(Open3270.TN3270.XMLScreenField field, TNEmulator emulator, string text)
        {
            emulator.SetText(text, field.Location.left, field.Location.top);
        }


        public void Write(string text)
        {

        }

        public void WriteLine(string text)
        {
        }
    }
}
