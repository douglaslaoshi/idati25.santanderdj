﻿using Santander.Automation.TN3270Bot.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Santander.Automation.TN3270Bot
{
    class Program
    {

        static void Main(string[] args)
        {
            CICS _mainframe = new CICS();

            var emulator = _mainframe.ConnectAndLogin(Properties.Settings.Default.User,
                                                      Properties.Settings.Default.Pass,
                                                      Properties.Settings.Default.Ip,
                                                      Properties.Settings.Default.Porta);
            while (true)
            {
                try
                {
                    List<Contrato> contratos = _mainframe.AutorizarDispensaJuros(emulator);
                    foreach (var contrato in contratos)
                    {
                        WriteLogContractSuccess(contrato, DateTime.Now);
                    }
                }
                catch (Exception ex)
                {
                    WriteLogError(ex.Message + Environment.NewLine + ex.StackTrace, DateTime.Now);
                    emulator.Dispose();
                    emulator = _mainframe.ConnectAndLogin(Properties.Settings.Default.User,
                                                      Properties.Settings.Default.Pass,
                                                      Properties.Settings.Default.Ip,
                                                      Properties.Settings.Default.Porta);


                }
                Console.WriteLine("Tentativa " + DateTime.Now);
            }
        }
        static void WriteLogSuccess(string contract, DateTime date)
        {
            File.AppendAllLines(Path.Combine(Properties.Settings.Default.LogPathOperador, "LogAprovacoes.log"),
                                new[] { $"{contract};{date.ToString("dd-MM-yy hh:mm:ss")}" });
        }
        static void WriteLogContractSuccess(Contrato contrato, DateTime date)
        {
            File.AppendAllLines(Path.Combine(Properties.Settings.Default.LogPathOperador, "LogAprovacoes.log"),
                                new[] { $"{contrato.contrato.Trim()};{date.ToString("dd-MM-yy hh:mm:ss")};{"R$ " + contrato.desconto.Trim()};{contrato.motivo.Trim()};{contrato.taxa.Trim() + "%"};{contrato.decisao}" });
            File.AppendAllLines(Path.Combine(Properties.Settings.Default.LogPathAdministrador, "LogAprovacoes.log"),
                                new[] { $"{contrato.contrato.Trim()};{date.ToString("dd-MM-yy hh:mm:ss")};{"R$ " + contrato.desconto.Trim()};{contrato.motivo.Trim()};{contrato.taxa.Trim() + "%"};{contrato.decisao}" });

        }
        static void WriteLogError(string message, DateTime date)
        {
            File.AppendAllLines(Path.Combine(Properties.Settings.Default.LogError, "LogErro.log"),
                                new[] { $"{message};{date.ToString("dd-MM-yy hh:mm:ss")}" });
        }


    }
}
